EntiRe
======

Entity Retrieval benchmarking code. Indexing of standard publicly available
entity retrieval test collections, and implementations of various models from
the literature.

Installing necessary Python packages
------------------------------------
Here's how we did it:

- install anaconda Python, academic license, pro version (continuum.io)
- conda create -n entire python numpy pip
- source activate entire
- pip install https://pypi.python.org/packages/source/l/langid/langid-1.1.4dev.tar.gz#md5=16a7f43e482562dbebabf1c6bbf7cf8d
- pip install cchardet (gave a couple of warnings, but installed succesfully)
- source deactivate (when you are done) 
